package Figures.Triangle;

import Figures.Figure;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TwoSideAngleTriangle extends Triangle implements Figure {
    private double dSideA;
    private double dSideB;
    private double dRadians;

    public TwoSideAngleTriangle(double dSideA, double dSideB, double dAngleAB) {
        super(dSideA, dSideB, dAngleAB);
        this.dSideA = dSideA;
        this.dSideB = dSideB;
        dRadians = Math.toRadians(dAngleAB);
    }

    public double getArea() {

        double dArea = dSideA * dSideB / 2 * Math.sin(dRadians);

        BigDecimal bd = new BigDecimal(dArea).setScale(2, RoundingMode.HALF_UP);

        return bd.doubleValue();
    }

    @Override
    public String toString() {
        return super.toString() + " Area is " + this.getArea();
    }
}
