package Figures.Triangle;

import Figures.Figure;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class HeightSideTriangle extends Triangle implements Figure {
    private double dSide;
    private double dHeight;

    public HeightSideTriangle(double dSide, double dHeight) {
        super(dSide, dHeight);
        this.dSide = dSide;
        this.dHeight = dHeight;
    }

    public double getArea() {

        double dArea = dSide * dHeight / 2;

        BigDecimal bd = new BigDecimal(dArea).setScale(2, RoundingMode.HALF_UP);

        return bd.doubleValue();
    }

    @Override
    public String toString() {
        return super.toString() + " Area is " + this.getArea();
    }
}
