package Figures.Triangle;

public abstract class Triangle {
    private double dSideA;
    private double dSideB;
    private double dSideC;
    private double dAngleAB;

    public Triangle(double dSideA, double dSideB) {
        this.dSideA = dSideA;
        this.dSideB = dSideB;
    }

    public Triangle(double dSideA, double dSideB, double dSideC) {
        this.dSideA = dSideA;
        this.dSideB = dSideB;
        this.dSideC = dSideC;
    }

    public String toString(){
        return this.getClass().getSimpleName();
    }
}
