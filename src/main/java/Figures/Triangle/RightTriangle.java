package Figures.Triangle;

import Figures.Figure;

public class RightTriangle extends Triangle implements Figure {

    private double dLengthA;
    private double dLengthB;

    public RightTriangle(double dLengthA, double dLengthB) {
        super(dLengthA, dLengthB);
        this.dLengthA = dLengthA;
        this.dLengthB = dLengthB;
    }

    public double getArea() {
        return 0.5 * dLengthA * dLengthB;
    }

    @Override
    public String toString() {
        return super.toString() + " Area is " + this.getArea();
    }
}
