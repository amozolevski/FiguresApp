package Figures.Triangle;

import Figures.Figure;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SimpleTriangle extends Triangle implements Figure {

    private double dLengthA;
    private double dLengthB;
    private double dLengthC;

    public SimpleTriangle(double dLengthA, double dLengthB, double dLengthC) {
        super(dLengthA, dLengthB, dLengthC);
        this.dLengthA = dLengthA;
        this.dLengthB = dLengthB;
        this.dLengthC = dLengthC;
    }

    public double getArea() {

        double p = (dLengthA + dLengthB + dLengthC) / 2;

        double dArea = p * Math.sqrt((p - dLengthA) * (p - dLengthB) * (p - dLengthC));

        //format value for two numbers after point
        BigDecimal bd = new BigDecimal(dArea).setScale(2, RoundingMode.HALF_UP);

        return bd.doubleValue();
    }

    @Override
    public String toString() {
        return super.toString() + " Area is " + this.getArea();
    }
}