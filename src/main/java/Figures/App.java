package Figures;

import Figures.Triangle.*;

public class App {
    public static void main(String[] args) {
        Triangle simple = new SimpleTriangle(3, 3, 4);
        Triangle right = new RightTriangle(5, 4);
        Triangle twoSideAngle = new TwoSideAngleTriangle(3, 4, 45);
        Triangle heightSide = new HeightSideTriangle(4, 6);

        System.out.println(simple);
        System.out.println(right);
        System.out.println(twoSideAngle);
        System.out.println(heightSide);

    }
}
